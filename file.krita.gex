PK     �FU               file.krita/PK
     �DBU5"k�g  g     file.krita/file-krita.py#!/usr/bin/env python3

# GIMP Plug-in for the Krita file format
# https://docs.krita.org/en/general_concepts/file_formats/file_kra.html
# https://invent.kde.org/graphics/krita/-/blob/master/krita/dtd/krita.dtd
# https://community.kde.org/Krita/Tile_Data_Format

# Copyright (C) 2009 by Jon Nordby <jononor@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# Based on MyPaint source code by Martin Renold
# http://gitorious.org/mypaint/mypaint/blobs/edd84bcc1e091d0d56aa6d26637aa8a925987b6a/lib/document.py

import gi
gi.require_version('Gimp', '3.0')
from gi.repository import Gimp
gi.require_version('Gegl', '0.4')
from gi.repository import Gegl
from gi.repository import GObject
from gi.repository import GLib
from gi.repository import Gio

import os, sys, tempfile, zipfile
import xml.etree.ElementTree as ET

def reverse_map(mapping):
    return dict((v,k) for k, v in mapping.items())

def get_image_attributes(krafile):
    xml = krafile.read('maindoc.xml')
    doc = ET.fromstring(xml)
    image = doc.find('{http://www.calligra.org/DTD/krita}IMAGE')
    layers = image.find('{http://www.calligra.org/DTD/krita}layers')
    w = int(image.attrib.get('width', ''))
    h = int(image.attrib.get('height', ''))
    name = image.attrib.get('name', '')
    x_res = int(image.attrib.get('x-res', '-1'))
    y_res = int(image.attrib.get('y-res', '-1'))

    return x_res, y_res, name, layers, w, h

def thumbnail_kra(procedure, file, thumb_size, args, data):
    tempdir = tempfile.mkdtemp('gimp-plugin-file-krita')
    krafile = zipfile.ZipFile(file.peek_path())
    x_res, y_res, name, layers, w, h = get_image_attributes(krafile)

    # create temp file
    tmp = os.path.join(tempdir, 'tmp.png')
    with open(tmp, 'wb') as fid:
        fid.write(krafile.read('preview.png'))

    thumb_file = Gio.file_new_for_path(tmp)
    result = Gimp.get_pdb().run_procedure('file-png-load', [
        GObject.Value(Gimp.RunMode, Gimp.RunMode.NONINTERACTIVE),
        GObject.Value(Gio.File, thumb_file),
    ])
    os.remove(tmp)
    os.rmdir(tempdir)

    if (result.index(0) == Gimp.PDBStatusType.SUCCESS):
        img = result.index(1)
        # TODO: scaling

        return Gimp.ValueArray.new_from_values([
            GObject.Value(Gimp.PDBStatusType, Gimp.PDBStatusType.SUCCESS),
            GObject.Value(Gimp.Image, img),
            GObject.Value(GObject.TYPE_INT, w),
            GObject.Value(GObject.TYPE_INT, h),
            GObject.Value(Gimp.ImageType, Gimp.ImageType.RGB_IMAGE),
            GObject.Value(GObject.TYPE_INT, 1)
        ])
    else:
        return procedure.new_return_values(result.index(0), GLib.Error(result.index(1)))

def load_kra(procedure, run_mode, file, args, data):
    tempdir = tempfile.mkdtemp('gimp-plugin-file-krita')
    krafile = zipfile.ZipFile(file.peek_path())
    x_res, y_res, name, layers, w, h = get_image_attributes(krafile)

    Gimp.progress_init("Loading Krita merged image")

    img = Gimp.Image.new(w, h, Gimp.ImageBaseType.RGB)
    img.set_file (file)

    # use the filename without extension as name
    n = os.path.basename(name)
    layer_name = os.path.splitext(n)[0]

    # create temp file
    tmp = os.path.join(tempdir, 'tmp.png')
    with open(tmp, 'wb') as fid:
        fid.write(krafile.read('mergedimage.png'))

    # import layer, set attributes and add to image
    result = gimp_layer = Gimp.get_pdb().run_procedure('gimp-file-load-layer', [
        GObject.Value(Gimp.RunMode, Gimp.RunMode.NONINTERACTIVE),
        GObject.Value(Gimp.Image, img),
        GObject.Value(Gio.File, Gio.File.new_for_path(tmp)),
    ])
    if (result.index(0) == Gimp.PDBStatusType.SUCCESS):
        gimp_layer = gimp_layer.index(1)
        os.remove(tmp)
    else:
        print("Error loading layer from Krita image.")

    gimp_layer.set_name(layer_name)
    gimp_layer.set_mode(Gimp.LayerMode.NORMAL)
    gimp_layer.set_opacity(100)
    gimp_layer.set_visible(True)

    img.insert_layer(gimp_layer, None, 1)
    # Assign resolution if valid values were saved
    if (x_res > -1 and y_res > -1):
        img.set_resolution(x_res, y_res)

    Gimp.progress_update(1)

    Gimp.progress_end()

    os.rmdir(tempdir)

    return Gimp.ValueArray.new_from_values([
        GObject.Value(Gimp.PDBStatusType, Gimp.PDBStatusType.SUCCESS),
        GObject.Value(Gimp.Image, img),
    ])


class FileKrita (Gimp.PlugIn):
    ## GimpPlugIn virtual methods ##
    def do_set_i18n(self, procname):
        return True, 'gimp30-python', None

    def do_query_procedures(self):
        return [ 'file-krita-load-thumb',
                 'file-krita-load' ]

    def do_create_procedure(self, name):
        if name == 'file-krita-load':
            procedure = Gimp.LoadProcedure.new (self, name,
                                                Gimp.PDBProcType.PLUGIN,
                                                load_kra, None)
            procedure.set_menu_label('Krita')
            procedure.set_documentation ('Loads a Krita (.kra) file (merged image only)',
                                         'Loads a Krita (.kra) file (merged image only)',
                                         name)
            procedure.set_mime_types ("application/x-kra");
            procedure.set_extensions ("kra");
            procedure.set_thumbnail_loader ('file-krita-load-thumb');
        else: # 'file-krita-load-thumb'
            procedure = Gimp.ThumbnailProcedure.new (self, name,
                                                     Gimp.PDBProcType.PLUGIN,
                                                     thumbnail_kra, None)
            procedure.set_documentation ('Loads a thumbnail from a Krita (.kra) file',
                                         'Loads a thumbnail from a Krita (.kra) file',
                                         name)
        procedure.set_attribution('Jon Nordby', #author
                                  'Jon Nordby', #copyright
                                  '2009') #year
        return procedure

Gimp.main(FileKrita.__gtype__, sys.argv)
PK
     ܶFU7	�1V  V  "   file.krita/file.krita.metainfo.xml<?xml version="1.0" encoding="UTF-8"?>
<component type="addon">
  <id>file.krita</id>
  <extends>org.gimp.GIMP</extends>
  <name>Krita File Loader</name>
  <summary>Load .kra project files (Preview)</summary>
  <description>
    <p>
    This extension loads the preview image from a .kra project file.
    This allows you to import and edit a merged image from Krita.
    Future versions of this extension will import more features.
    </p>
  </description>
  <url type="homepage">https://gimp.org</url>
  <metadata_license>CC0-1.0</metadata_license>
  <project_license>GPL-3.0+</project_license>
  <releases>
   <release version="1.0.0" date="2022-10-05" />
  </releases>
  <requires>
   <id version="2.99.0" compare="ge">org.gimp.GIMP</id>
  </requires>
  <metadata>
   <value key="GIMP::plug-in-path">file-krita.py</value>
  </metadata>
</component>
PK?      �FU             $              file.krita/
         K�,7���;F7�����~����PK? 
     �DBU5"k�g  g   $           )   file.krita/file-krita.py
         I�x\��jp������eN�PK? 
     ܶFU7	�1V  V  " $           �  file.krita/file.krita.metainfo.xml
         �,�-����m��������PK      ;  \    